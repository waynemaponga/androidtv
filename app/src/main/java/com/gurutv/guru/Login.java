package com.gurutv.guru;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.gurutv.guru.com.gurutv.dto.AppController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {
    String url = "http://10.0.2.2:3000/findme";
    private String TAG = Login.class.getSimpleName();
    private EditText email;
    //Creating a broadcast receiver for gcm registration
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = (EditText) findViewById(R.id.edtxemail);
        StrictMode.ThreadPolicy policy = new
                StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }


    public void checkUser(View view) throws JSONException {
        final Gson gson = new Gson();
        RequestQueue queue = Volley.newRequestQueue(this);
        final String emailtxt = email.getText().toString().toLowerCase();

        if (emailtxt.isEmpty() || emailtxt.length() == 0 || emailtxt.equals("") || emailtxt == null) {
            Toast.makeText(getApplicationContext(), "Please  enter your email ", Toast.LENGTH_SHORT).show();
        } else {
            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, null,
                    new Response.Listener<JSONObject>() {



                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, response.toString());


                            try {
                             String json = gson.toJson(response.get("message"));
                                Log.d(TAG, json.toString());
                                if (json.toString().contains("notfound")) {
                                    Intent intent = new Intent(Login.this, Register.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "notfound ", Toast.LENGTH_SHORT).show();
                                }

                                if (response.toString().contains("exist")) {
                                    Intent intent = new Intent(Login.this, Registered.class);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "exist ", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }



                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    VolleyLog.d(TAG, "Error: " + error.getMessage());

                }
            }) {


                /**
                 * Passing some request headers
                 */
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put("email", emailtxt);
                    return params;
                }

            };

            AppController.getInstance().getRequestQueue().add(jsonObjReq);
        }
    }
}