package com.gurutv.guru.com.gurutv.dto.com.gurutv.util;

public class URLs {
    private static final String ROOT_URL = "http://10.0.2.2:3000";
    public static final String Find_ME = ROOT_URL + "/findme";
    public static final String URL_LOGIN= ROOT_URL + "login";
    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "ah_firebase";

}
