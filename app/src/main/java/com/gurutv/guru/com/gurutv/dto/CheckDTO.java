package com.gurutv.guru.com.gurutv.dto;

import com.google.gson.annotations.SerializedName;

public class CheckDTO {
    @SerializedName("message")
   public  String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
